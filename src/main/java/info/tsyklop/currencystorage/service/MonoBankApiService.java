package info.tsyklop.currencystorage.service;

import info.tsyklop.currencystorage.pojo.CurrencyInfo;

import java.util.List;
import java.util.Optional;

public interface MonoBankApiService {

    String BASE_URL = "https://api.monobank.ua/";

    String CURRENCY_LIST_URL = BASE_URL + "bank/currency";

    List<CurrencyInfo> getAllCurrencies();
    Optional<CurrencyInfo> getCurrencyByCode(Integer code);

}
