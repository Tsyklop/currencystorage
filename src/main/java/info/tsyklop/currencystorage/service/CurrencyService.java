package info.tsyklop.currencystorage.service;

import info.tsyklop.currencystorage.pojo.CurrencyDTO;
import org.springframework.http.ResponseEntity;

public interface CurrencyService {
    ResponseEntity get(String mnemonic);
    ResponseEntity create(CurrencyDTO currency);
}
