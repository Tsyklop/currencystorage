package info.tsyklop.currencystorage.service.impl;

import info.tsyklop.currencystorage.persistence.entity.CurrencyEntity;
import info.tsyklop.currencystorage.persistence.entity.CurrencyJournalEntity;
import info.tsyklop.currencystorage.persistence.repository.CurrencyJournalRepository;
import info.tsyklop.currencystorage.persistence.repository.CurrencyRepository;
import info.tsyklop.currencystorage.pojo.CurrencyDTO;
import info.tsyklop.currencystorage.pojo.CurrencyInfo;
import info.tsyklop.currencystorage.pojo.CurrencyJournalDTO;
import info.tsyklop.currencystorage.service.CurrencyService;
import info.tsyklop.currencystorage.service.MonoBankApiService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final ModelMapper modelMapper;

    private final MonoBankApiService monoBankApiService;

    private final CurrencyRepository currencyRepository;
    private final CurrencyJournalRepository currencyJournalRepository;

    @Override
    @Transactional
    public ResponseEntity get(String mnemonic) {

        Optional<CurrencyEntity> currencyOptional = this.currencyRepository.findByMnemonic(mnemonic);

        if (!currencyOptional.isPresent()) {
            return ResponseEntity.badRequest().body("Currency with mnemonic '" + mnemonic + "' not found");
        }

        Optional<CurrencyJournalEntity> currencyJournalOptional = this.currencyJournalRepository.findByCurrencyAndDate(currencyOptional.get(), LocalDate.now());

        if (currencyJournalOptional.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(currencyJournalOptional.get(), CurrencyJournalDTO.class));
        }

        Optional<CurrencyInfo> currencyInfoOptional = monoBankApiService.getCurrencyByCode(currencyOptional.get().getCode());

        if (!currencyInfoOptional.isPresent()) {
            return ResponseEntity.badRequest().body("Cannot find currency");
        }

        CurrencyJournalEntity currencyJournal = CurrencyJournalEntity.builder()
                .date(LocalDate.now())
                .sellingRate(currencyInfoOptional.get().getRateSell())
                .purchaseRate(currencyInfoOptional.get().getRateBuy())
                .currency(currencyOptional.get())
                .build();

        this.currencyJournalRepository.save(currencyJournal);

        return ResponseEntity.ok(modelMapper.map(currencyJournal, CurrencyJournalDTO.class));

    }

    @Override
    public ResponseEntity create(CurrencyDTO currency) {

        if (this.currencyRepository.existsByCode(currency.getCode())) {
            return ResponseEntity.badRequest().body("Currency with code '" + currency.getCode() + "' already exists");
        }

        if (this.currencyRepository.existsByMnemonic(currency.getMnemonic())) {
            return ResponseEntity.badRequest().body("Currency with mnemonic '" + currency.getMnemonic() + "' already exists");
        }

        CurrencyEntity currencyEntity = CurrencyEntity.builder()
                .code(currency.getCode())
                .mnemonic(currency.getMnemonic())
                .description(currency.getDescription())
                .build();

        this.currencyRepository.save(currencyEntity);

        return ResponseEntity.status(HttpStatus.CREATED).body("Currency successfully created");
    }

}
