package info.tsyklop.currencystorage.service.impl;

import info.tsyklop.currencystorage.pojo.CurrencyInfo;
import info.tsyklop.currencystorage.service.MonoBankApiService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MonoBankApiServiceImpl implements MonoBankApiService {

    private final ModelMapper modelMapper;
    private final RestTemplate restTemplate;

    @Override
    public List<CurrencyInfo> getAllCurrencies() {
        return restTemplate.exchange(CURRENCY_LIST_URL, HttpMethod.GET, null, new ParameterizedTypeReference<List<CurrencyInfo>>(){}).getBody();
    }

    @Override
    public Optional<CurrencyInfo> getCurrencyByCode(Integer code) {
        return getAllCurrencies().stream().filter(currencyInfo -> currencyInfo.getCurrencyCodeA() == code).findFirst();
    }

}
