package info.tsyklop.currencystorage.service.impl;

import info.tsyklop.currencystorage.config.SecurityTokenUtils;
import info.tsyklop.currencystorage.persistence.entity.UserEntity;
import info.tsyklop.currencystorage.persistence.repository.UserRepository;
import info.tsyklop.currencystorage.pojo.*;
import info.tsyklop.currencystorage.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final ModelMapper modelMapper;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final SecurityTokenUtils securityTokenUtils;

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity signIn(UserAuthDTO signIn) {

        Optional<UserEntity> user = this.userRepository.findByEmail(signIn.getEmail());

        if(!user.isPresent() || !this.passwordEncoder.matches(signIn.getPassword(), user.get().getPassword())) {
            return ResponseEntity.badRequest().body("Email or password incorrect");
        }

        Map<String, Object> data = createUserData(user.get());

        return ResponseEntity.ok(
                AuthDTO.builder()
                .user(this.modelMapper.map(user.get(), UserDTO.class))
                .token(TokenDTO.builder()
                        .access(this.securityTokenUtils.createToken(data, false))
                        .refresh(this.securityTokenUtils.createToken(data, true))
                        .build())
                .build()
        );

    }

    @Override
    @Transactional
    public ResponseEntity signUp(UserAuthDTO signUp) {

        if(this.userRepository.existsByEmail(signUp.getEmail())) {
            return ResponseEntity.badRequest().body("Email already exists");
        }

        UserEntity user = UserEntity.builder()
                .email(signUp.getEmail())
                .password(this.passwordEncoder.encode(signUp.getPassword()))
                .build();

        this.userRepository.save(user);

        return ResponseEntity.ok("You successfully registered");
    }

    private Map<String, Object> createUserData(UserEntity user) {
        Map<String, Object> data = new HashMap<>();
        data.put("email", user.getEmail());
        return data;
    }

}
