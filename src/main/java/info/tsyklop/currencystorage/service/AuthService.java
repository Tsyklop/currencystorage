package info.tsyklop.currencystorage.service;

import info.tsyklop.currencystorage.pojo.UserAuthDTO;
import org.springframework.http.ResponseEntity;

public interface AuthService {
    ResponseEntity signIn(UserAuthDTO signIn);
    ResponseEntity signUp(UserAuthDTO signUp);
}
