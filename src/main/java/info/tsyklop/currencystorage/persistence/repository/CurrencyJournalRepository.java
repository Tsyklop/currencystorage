package info.tsyklop.currencystorage.persistence.repository;

import info.tsyklop.currencystorage.persistence.entity.CurrencyEntity;
import info.tsyklop.currencystorage.persistence.entity.CurrencyJournalEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface CurrencyJournalRepository extends JpaRepository<CurrencyJournalEntity, Long> {

    List<CurrencyJournalEntity> findAllByCurrency(CurrencyEntity currency);

    Optional<CurrencyJournalEntity> findByCurrencyAndDate(CurrencyEntity currency, LocalDate date);

}
