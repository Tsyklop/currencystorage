package info.tsyklop.currencystorage.persistence.repository;

import info.tsyklop.currencystorage.persistence.entity.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long> {
    boolean existsByCode(Integer code);
    boolean existsByMnemonic(String mnemonic);
    Optional<CurrencyEntity> findByMnemonic(String mnemonic);
}
