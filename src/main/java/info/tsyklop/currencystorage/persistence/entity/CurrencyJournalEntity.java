package info.tsyklop.currencystorage.persistence.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Table(name = "currencyJournal")
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyJournalEntity extends AbstractEntity {

    private LocalDate date;

    @Column(name = "sellingRate", nullable = false, updatable = false)
    private Double sellingRate;

    @Column(name = "purchaseRate", nullable = false, updatable = false)
    private Double purchaseRate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = CurrencyEntity.class)
    private CurrencyEntity currency;

}
