package info.tsyklop.currencystorage.persistence.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name="user")
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity extends AbstractEntity {

    private String email;
    private String password;

}
