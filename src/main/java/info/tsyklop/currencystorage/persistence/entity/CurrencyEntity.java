package info.tsyklop.currencystorage.persistence.entity;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name = "currency")
@Entity
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyEntity extends AbstractEntity {

    @Column(name = "code", nullable = false, updatable = false)
    private Integer code;

    @Column(name = "mnemonic", nullable = false, updatable = false)
    private String mnemonic;

    @Type(type="text")
    @Column(name = "description")
    private String description;

}
