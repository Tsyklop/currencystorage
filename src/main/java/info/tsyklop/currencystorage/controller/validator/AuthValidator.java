package info.tsyklop.currencystorage.controller.validator;

import info.tsyklop.currencystorage.pojo.UserAuthDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class AuthValidator implements Validator {

    protected static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Override
    public boolean supports(Class<?> clazz) {
        return UserAuthDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        UserAuthDTO userAuthDTO = (UserAuthDTO) target;

        if(StringUtils.isBlank(userAuthDTO.getEmail()) || !userAuthDTO.getEmail().matches(EMAIL_PATTERN)) {
            errors.reject("email", "Email incorrect");
        }

        if(StringUtils.isBlank(userAuthDTO.getPassword()) || userAuthDTO.getPassword().length() <= 3) {
            errors.reject("password", "Password incorrect");
        }

    }

}
