package info.tsyklop.currencystorage.controller.validator;

import info.tsyklop.currencystorage.pojo.CurrencyDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component("currencyValidator")
public class CurrencyValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return CurrencyDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        CurrencyDTO currency = (CurrencyDTO) target;

        if (Objects.isNull(currency.getCode())) {
            errors.reject( "code.null", "Code cannot be null");
        }

        if (Objects.isNull(currency.getMnemonic())) {
            errors.reject("mnemonic.null", "Mnemonic cannot be null");
        }

        if (Objects.isNull(currency.getDescription())) {
            errors.reject("description.null", "Description cannot be null");
        }

    }

}
