package info.tsyklop.currencystorage.controller;

import info.tsyklop.currencystorage.controller.validator.AuthValidator;
import info.tsyklop.currencystorage.pojo.UserAuthDTO;
import info.tsyklop.currencystorage.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/auth", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class AuthRestController extends AbstractRestController {

    private final AuthService authService;

    @InitBinder()
    protected void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new AuthValidator());
    }

    @PostMapping("/signIn")
    public ResponseEntity signIn(@Valid @RequestBody UserAuthDTO signIn, Errors errors) {
        if(errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }
        return this.authService.signIn(signIn);
    }

    @PostMapping("/signUp")
    public ResponseEntity signUp(@Valid @RequestBody UserAuthDTO signUp, Errors errors) {
        if(errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }
        return this.authService.signUp(signUp);
    }

}
