package info.tsyklop.currencystorage.controller;

import info.tsyklop.currencystorage.controller.validator.CurrencyValidator;
import info.tsyklop.currencystorage.pojo.CurrencyDTO;
import info.tsyklop.currencystorage.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Secured("ROLE_USER")
@RestController
@RequestMapping(value = "/api/v1/currency", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class CurrencyRestController extends AbstractRestController {

    private final CurrencyService currencyService;

    @Qualifier("currencyValidator")
    private final CurrencyValidator currencyValidator;

    @InitBinder
    protected void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.setValidator(this.currencyValidator);
    }

    @GetMapping
    public ResponseEntity getRequestParameter(@RequestParam("mnemonic") String mnemonic) {
        return currencyService.get(mnemonic);
    }

    @GetMapping("/{mnemonic}")
    public ResponseEntity getUrlParameter(@PathVariable("mnemonic") String mnemonic) {
        return currencyService.get(mnemonic);
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody CurrencyDTO currency, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }
        return this.currencyService.create(currency);
    }

}
