package info.tsyklop.currencystorage.pojo;

import lombok.Data;

@Data
public class SignUpDTO {

    private String email;

    private String password;

}
