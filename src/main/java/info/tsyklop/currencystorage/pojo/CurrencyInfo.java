package info.tsyklop.currencystorage.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyInfo {

    private int date;

    private int currencyCodeA;
    private int currencyCodeB;

    private double rateBuy;
    private double rateSell;

    private double rateCross;

}
