package info.tsyklop.currencystorage.pojo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TokenDTO {

    private String access;
    private String refresh;

}
