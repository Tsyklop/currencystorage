package info.tsyklop.currencystorage.pojo;

import lombok.Data;

@Data
public class CurrencyDTO {

    private Integer code;

    private String mnemonic;

    private String description;

}
