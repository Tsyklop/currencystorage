package info.tsyklop.currencystorage.pojo;

import lombok.Data;

@Data
public class UserAuthDTO {

    private String email;
    private String password;

}
