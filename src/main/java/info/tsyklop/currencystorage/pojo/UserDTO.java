package info.tsyklop.currencystorage.pojo;

import lombok.Data;

@Data
public class UserDTO {

    private String email;

}
