package info.tsyklop.currencystorage.pojo;

import lombok.Data;

@Data
public class SignInDTO {

    private String email;
    private String password;

}
