package info.tsyklop.currencystorage.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CurrencyJournalDTO {

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    private Double sellingRate;

    private Double purchaseRate;

}
