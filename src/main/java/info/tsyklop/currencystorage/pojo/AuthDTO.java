package info.tsyklop.currencystorage.pojo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthDTO {

    private UserDTO user;

    private TokenDTO token;

}
