package info.tsyklop.currencystorage.config;

import io.jsonwebtoken.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Getter
@Setter
@Component
@Configuration
@ConfigurationProperties(prefix = "security.token")
public class SecurityTokenUtils {

    private String issuer;

    private int clockSkewSec;

    private String secretKey;

    private String authPrefix;

    private int accessExpireLength;

    private int refreshExpireLength;

    @PostConstruct
    public void postConstruct() {
        this.secretKey = Base64.getEncoder().encodeToString(this.secretKey.getBytes());
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityTokenUtils.class);

    public String createToken(Map<String, Object> userData, boolean isRefresh) {

        Claims claims = createDefaultClaims(userData);

        if (isRefresh) {
            claims.put("refresh", true);
        }

        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.add(Calendar.MILLISECOND, isRefresh ? refreshExpireLength : accessExpireLength);

        return compact(claims, now, calendar.getTime());

    }

    public String resolveToken(String token) {
        if (token != null && token.startsWith(this.authPrefix)) {
            return token.substring(this.authPrefix.length() + 1);
        }
        return null;
    }

    public String resolveToken(HttpServletRequest req) {
        return resolveToken(req.getHeader(AUTHORIZATION));
    }

    public String get(String key, String token) {
        try {
            return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get(key, String.class);
        } catch (JwtException | IllegalArgumentException e) {
            LOGGER.error("EMAIL NOT FOUND IN TOKEN " + token, e);
        }
        return null;
    }

    public Claims getTokenBody(String token) {
        try {
            return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
        } catch (JwtException | IllegalArgumentException e) {
            LOGGER.error("GETTING BODY FROM TOKEN ERROR" + token, e);
        }
        return null;
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser()
                    .setSigningKey(this.secretKey)
                    .requireIssuer(this.issuer)
                    .parseClaimsJws(token);
            return true;
        } catch (SignatureException ex) {
            LOGGER.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            LOGGER.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            LOGGER.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            LOGGER.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            LOGGER.error("JWT claims string is empty.");
        } catch (Exception e) {
            LOGGER.error("UNKNOWN ERROR", e);
        }
        return false;
    }

    private Claims createDefaultClaims(Map<String, Object> userData) {
        return Jwts.claims(userData).setSubject(RandomStringUtils.randomAlphanumeric(10));
    }

    private String compact(Claims claims, Date issuedAt, Date expiration) {
        return Jwts.builder()
                .setClaims(claims)
                .setIssuer(this.issuer)
                .setIssuedAt(issuedAt)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS256, this.secretKey)
                .compact();
    }

}
