package info.tsyklop.currencystorage.model;

public enum StatusType {
    SUCCESS, UNAUTHORIZED, FAILURE, ERROR, NOT_FOUND, BAD_REQUEST;
}
