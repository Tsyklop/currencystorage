package info.tsyklop.currencystorage.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import static info.tsyklop.currencystorage.model.StatusType.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestApiResponse<D> {

    private D data;

    private String message;

    private HttpStatus code;

    @Builder.Default
    private StatusType status = SUCCESS;

    public int getCode() {
        return this.code.value();
    }

    public static RestApiResponse success(String message) {
        return RestApiResponse.builder()
                .status(SUCCESS)
                .code(HttpStatus.OK)
                .message(message)
                .build();
    }

    public static <D> RestApiResponse successWithData(D data) {
        return RestApiResponse.builder()
                .status(SUCCESS)
                .code(HttpStatus.OK)
                .data(data)
                .build();
    }

    public static <D> RestApiResponse success(String message, D data) {
        return RestApiResponse
                .builder()
                .data(data)
                .status(SUCCESS)
                .message(message)
                .code(HttpStatus.OK)
                .build();
    }

    public static RestApiResponse error(String message) {
        return RestApiResponse.builder()
                .status(ERROR)
                .message(message)
                .code(HttpStatus.INTERNAL_SERVER_ERROR)
                .build();
    }

    public static RestApiResponse failure(String message) {
        return RestApiResponse.builder()
                .status(FAILURE)
                .message(message)
                .code(HttpStatus.INTERNAL_SERVER_ERROR)
                .build();
    }

    public static RestApiResponse unauthorized() {
        return unauthorized(null);
    }

    public static RestApiResponse unauthorized(String message) {
        return  RestApiResponse.builder()
                .message(message)
                .code(HttpStatus.UNAUTHORIZED)
                .status(StatusType.UNAUTHORIZED)
                .build();
    }

}
